// Generated from ProLog.g4 by ANTLR 4.4

  package jtwbprolog.parser; 

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ProLogParser}.
 */
public interface ProLogListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ProLogParser#clause}.
	 * @param ctx the parse tree
	 */
	void enterClause(@NotNull ProLogParser.ClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProLogParser#clause}.
	 * @param ctx the parse tree
	 */
	void exitClause(@NotNull ProLogParser.ClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProLogParser#argument}.
	 * @param ctx the parse tree
	 */
	void enterArgument(@NotNull ProLogParser.ArgumentContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProLogParser#argument}.
	 * @param ctx the parse tree
	 */
	void exitArgument(@NotNull ProLogParser.ArgumentContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProLogParser#goal}.
	 * @param ctx the parse tree
	 */
	void enterGoal(@NotNull ProLogParser.GoalContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProLogParser#goal}.
	 * @param ctx the parse tree
	 */
	void exitGoal(@NotNull ProLogParser.GoalContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProLogParser#definiteClause}.
	 * @param ctx the parse tree
	 */
	void enterDefiniteClause(@NotNull ProLogParser.DefiniteClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProLogParser#definiteClause}.
	 * @param ctx the parse tree
	 */
	void exitDefiniteClause(@NotNull ProLogParser.DefiniteClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProLogParser#startingRule}.
	 * @param ctx the parse tree
	 */
	void enterStartingRule(@NotNull ProLogParser.StartingRuleContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProLogParser#startingRule}.
	 * @param ctx the parse tree
	 */
	void exitStartingRule(@NotNull ProLogParser.StartingRuleContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProLogParser#functionName}.
	 * @param ctx the parse tree
	 */
	void enterFunctionName(@NotNull ProLogParser.FunctionNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProLogParser#functionName}.
	 * @param ctx the parse tree
	 */
	void exitFunctionName(@NotNull ProLogParser.FunctionNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProLogParser#functionOrConstant}.
	 * @param ctx the parse tree
	 */
	void enterFunctionOrConstant(@NotNull ProLogParser.FunctionOrConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProLogParser#functionOrConstant}.
	 * @param ctx the parse tree
	 */
	void exitFunctionOrConstant(@NotNull ProLogParser.FunctionOrConstantContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProLogParser#unitClause}.
	 * @param ctx the parse tree
	 */
	void enterUnitClause(@NotNull ProLogParser.UnitClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProLogParser#unitClause}.
	 * @param ctx the parse tree
	 */
	void exitUnitClause(@NotNull ProLogParser.UnitClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProLogParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterLiteral(@NotNull ProLogParser.LiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProLogParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitLiteral(@NotNull ProLogParser.LiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProLogParser#conclusion}.
	 * @param ctx the parse tree
	 */
	void enterConclusion(@NotNull ProLogParser.ConclusionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProLogParser#conclusion}.
	 * @param ctx the parse tree
	 */
	void exitConclusion(@NotNull ProLogParser.ConclusionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProLogParser#predicate}.
	 * @param ctx the parse tree
	 */
	void enterPredicate(@NotNull ProLogParser.PredicateContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProLogParser#predicate}.
	 * @param ctx the parse tree
	 */
	void exitPredicate(@NotNull ProLogParser.PredicateContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProLogParser#premises}.
	 * @param ctx the parse tree
	 */
	void enterPremises(@NotNull ProLogParser.PremisesContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProLogParser#premises}.
	 * @param ctx the parse tree
	 */
	void exitPremises(@NotNull ProLogParser.PremisesContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProLogParser#variable}.
	 * @param ctx the parse tree
	 */
	void enterVariable(@NotNull ProLogParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProLogParser#variable}.
	 * @param ctx the parse tree
	 */
	void exitVariable(@NotNull ProLogParser.VariableContext ctx);
}