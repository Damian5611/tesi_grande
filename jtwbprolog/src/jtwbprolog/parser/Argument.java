package jtwbprolog.parser;

public abstract class Argument {

	protected String name;
	
	public String getName() {
		
		return name;
		
	}
	
  public void setName(String name) {
		
		this.name = name;
		
	}
	
	public String toString() {
		
		return name;
		
	}
	
}

