package jtwbprolog.parser;



import org.antlr.v4.runtime.DefaultErrorStrategy;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.misc.ParseCancellationException;

public class ErrorHandler extends DefaultErrorStrategy{

	private boolean error = false;
	
	@Override
    public void recover(Parser recognizer, RecognitionException e) {
       
		error = true;
        throw new ParseCancellationException(e);
    }


    @Override
    public Token recoverInline(Parser recognizer)
        throws RecognitionException
    {
       
    	
        error = true;
        throw new ParseCancellationException();
    }

    protected void reportUnwantedToken(Parser recognizer) {
    	
    	error = true;
    	
    }
    
    public boolean isWrong() {
    	
    	return error;
    	
    }
    
}
