package jtwbprolog.parser;

import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

public class PrintCode extends ProLogBaseListener {

	private String result = "";
	private boolean error = false;
	
	@Override
	public void visitTerminal(TerminalNode node) {
	
		if(!node.getText().equals("<EOF>"))
		    result += node.getText();
		
		if(node.getText().equals("."))
			result += "\n";
		
		else if(node.getText().equals(":-") || node.getText().equals(",") )
			result += " ";
	
	}
	
	
	public void visitErrorNode(ErrorNode node) {
		
		System.out.println("ERROR");
		error = true;
		
	}
	
	public void printResult() {
		
		if(!error)
		    System.out.println("\n" + result);
		
	}
	
	public boolean isWrong() {
		
		return error;
		
	}
	
}
