package jtwbprolog.parser;

import java.util.LinkedList;

public class Literal {
	
	private Predicate predicate;
	private LinkedList<Argument> arguments = new LinkedList<Argument>();

	public String getPredicateName() {
		
		return predicate.getName();
		
	}
	
	public  int getNumArgs() {
		
		return arguments.size();
		
	}
	
	public void setPredicate(Predicate predicate) {
		
		this.predicate = predicate;
		
	}
	
	public void addArgument(Argument argument) {
		
		arguments.add(argument);
		
	}
	
	public void changeToCostant() {
		
		Argument argument = arguments.getLast();
		Constant constant = new Constant(argument.getName());
		arguments.removeLast();
		arguments.add(constant);
		
	}
	
	public String toString() {
		
		String result = predicate.toString();
		result += "(";
		
		for(int i = 0; i < arguments.size(); i++) {
			
			result += arguments.get(i).toString();
			
			if(i + 1 != arguments.size())
				result += ",";
			
		}
		
		result += ")";
		
		return result;
		
	}
	
}
