package jtwbprolog.parser;

import java.util.LinkedList;

public class Program {

	private LinkedList<Clause> clauses = new LinkedList<Clause>();
	
	public void addClause(Clause c) {
		
		clauses.add(c);
		
	}
	
	public int getLines() {
		
		return clauses.size();
		
	}
	
	public String toString() {
		
        String result = "";
		
		for(int i = 0; i < clauses.size(); i++) {
			
			result +=i+1 + " " + clauses.get(i).toString();
			
			if(i + 1 != clauses.size())
				result += "\n";
			
		}
		
		return result;
		
	}
 	
}
