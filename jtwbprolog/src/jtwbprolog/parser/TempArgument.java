package jtwbprolog.parser;

import java.util.LinkedList;

import Jtabwb.parser.Argument;

public abstract class TempArgument {

	protected String name;
	
	public String getName() {
		
		return name;
		
	}
	
  public void setName(String name) {
		
		this.name = name;
		
	}
	
	public String toString() {
		
		return name;
		
	}
	
	public abstract boolean isFunction();
	
	public abstract boolean isVariable();
	
	public abstract LinkedList<TempArgument> getArgs();
	
}

