package jtwbprolog.parser;

public class DefiniteClause extends Clause {
	
	private Premises premises;

	public void setPremises(Premises premises) {
		
		this.premises = premises;
		
	}
	
	public String toString() {
		
		return conclusion.toString() + " :- " + premises.toString() + ".";
		
	}
}
