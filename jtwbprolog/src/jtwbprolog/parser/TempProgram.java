package jtwbprolog.parser;

import java.util.LinkedList;

public class TempProgram {

	private LinkedList<TempClause> clauses = new LinkedList<TempClause>();
	private TempGoal goal;
	
	public void addClause(TempClause c) {
		
		clauses.add(c);
		
	}
	
	public LinkedList<TempClause> getClauses() {
		
		return clauses;
		
	}
	
	public TempGoal getGoal() {
		
		return goal;
		
	}
	
	public void setGoal(TempGoal goal) {
		
		this.goal = goal;
		
	}
	
	public int getLines() {
		
		return clauses.size();
		
	}
	
	public String toString() {
		
        String result = "";
		
		for(int i = 0; i < clauses.size(); i++) {
			
			result +=i+1 + " " + clauses.get(i).toString();
			
			if(i + 1 != clauses.size())
				result += "\n";
			
		}
		
		result += "\n" + (clauses.size() + 1)  + goal.toString();
		
		return result;
		
	}
 	
}
