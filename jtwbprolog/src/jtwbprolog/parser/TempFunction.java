package jtwbprolog.parser;

import java.util.LinkedList;

public class TempFunction extends TempArgument implements toCheck {

	private LinkedList<TempArgument> arguments = new LinkedList<TempArgument>();
	
    public Type getType() {
		
		return Type.FUNCTION;	
	}
	
	public void addArgument(TempArgument argument) {
		
		arguments.add(argument);
		
	}
	
	public boolean isFunction() {
		
		return !arguments.isEmpty();
		
	}
	
	public boolean isVariable() {
		
		return false;
		
	}
		
   public void changeToCostant() {
		
		TempArgument argument = arguments.getLast();
		TempConstant constant = new TempConstant(argument.getName());
		arguments.removeLast();
		arguments.add(constant);
		
	}
   
   public  int getNumArgs() {
		
		return arguments.size();
		
	}
   
   public LinkedList<TempArgument> getArgs() {
	   
	   
	   return arguments;
	   
   }
	
	public String toString() {
		
		String result = super.toString();
		result += "(";
		
		for(int i = 0; i < arguments.size(); i++) {
			
			result += arguments.get(i).toString();
			
			if(i + 1 != arguments.size())
				result += ",";
			
		}
		
		result += ")";
		
		return result;
		
	}
	
}
