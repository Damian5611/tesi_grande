package jtwbprolog.parser;

public class TempPredicate {

	private String name;
	
	public void setName(String name) {
		
		this.name = name;
		
	}
	
	public String getName() {
		
		return name;
		
	}
	
	public String toString() {
		
		return name;
		
	}
	
}
