package jtwbprolog.parser;

abstract public class Clause {
	
	protected Conclusion conclusion;
	
	public void setConclusion(Conclusion conclusion) {
		
		this.conclusion = conclusion;
		
	}
	
	public void setPremises(Premises premises) { };
	
	public abstract String toString();

}
