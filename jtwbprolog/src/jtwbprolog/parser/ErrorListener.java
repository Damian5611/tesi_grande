package jtwbprolog.parser;


import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

public class ErrorListener extends BaseErrorListener{
	
	boolean error = false;
	
	public void syntaxError(Recognizer<?, ?> recognizer,
			Object offendingSymbol,
			int line, int charPositionInLine,
			String msg,
			RecognitionException e)
			{
			
		error = true;

}

	public boolean isWrong() {
		
		return error;
		
	}
	
	
}