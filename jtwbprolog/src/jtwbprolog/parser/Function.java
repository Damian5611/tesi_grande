package jtwbprolog.parser;

import java.util.LinkedList;

public class Function extends Argument {

	private LinkedList<Argument> arguments = new LinkedList<Argument>();
	
	public void addArgument(Argument argument) {
		
		arguments.add(argument);
		
	}
	
	public boolean isFunction() {
		
		return !arguments.isEmpty();
		
	}
		
   public void changeToCostant() {
		
		Argument argument = arguments.getLast();
		Constant constant = new Constant(argument.getName());
		arguments.removeLast();
		arguments.add(constant);
		
	}
   
   public  int getNumArgs() {
		
		return arguments.size();
		
	}
	
	public String toString() {
		
		String result = super.toString();
		result += "(";
		
		for(int i = 0; i < arguments.size(); i++) {
			
			result += arguments.get(i).toString();
			
			if(i + 1 != arguments.size())
				result += ",";
			
		}
		
		result += ")";
		
		return result;
		
	}
	
}
