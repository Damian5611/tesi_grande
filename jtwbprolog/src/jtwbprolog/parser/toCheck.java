package jtwbprolog.parser;

public interface toCheck {

	public enum Type {LITERAL, FUNCTION};
	
	public String getName();
	
	public int getNumArgs();
	
	public Type getType();
	
}
