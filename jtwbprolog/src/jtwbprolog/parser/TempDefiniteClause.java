package jtwbprolog.parser;

import java.util.LinkedList;

public class TempDefiniteClause extends TempClause {
	
	private Premises premises;

	public void setPremises(Premises premises) {
		
		this.premises = premises;
		
	}
	
	public boolean isUnit() {
		
		return false;
		
	}
	
   public LinkedList<TempLiteral> getPremises( ){
		
		return premises.getPremises();
		
	}
	
	public String toString() {
		
		return conclusion.toString() + " :- " + premises.toString() + ".";
		
	}
}
