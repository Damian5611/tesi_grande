package Jtabwb.parser;

public class Literal {

	Predicate predicate;
	Argument[] args;
	
	public Literal(Predicate predicate, int numArgs) {
		
		this.predicate = predicate;
		args = new Argument[numArgs];
		
	}
	
	public void addArg(Argument arg, int index) {
		
		args[index] = arg;
		
	}
	
   public String toString() {
		
		String result = predicate.getName();
		result += "(";
		
		for(int i = 0; i < args.length; i++) {
			
			result += args[i].toString();
			
			if(i + 1 != args.length)
				result += ",";
			
		}
		
		result += ")";
		
		return result;
		
	}
	
}
