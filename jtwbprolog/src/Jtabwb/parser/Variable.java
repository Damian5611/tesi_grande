package Jtabwb.parser;

public class Variable implements Argument {
	
	private String name;
	
	public Variable(String name) {
		
		this.name = name; 
		
	}
	
	public String toString() {
		
		return name;
		
	}
	
	public boolean isFunction() {
		
		return false;
		
	}
	
	public boolean isVariable() {
		
		return true;
		
	}
	
	public void addArgument(Argument arg, int index) {};

}
