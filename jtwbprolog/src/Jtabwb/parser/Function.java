package Jtabwb.parser;

public class Function implements Argument{

	private String name;
	private int numArgs;
	
	public Function(String name, int numArgs) {
		
		this.name = name;
		this.numArgs = numArgs;
		
	}
	
	public boolean isFunction() {
		
		return false;
		
	}
	
	public boolean isVariable() {
		
		return false;
			
	}

	public String getName() {
		
		return name;
		
	}
	
	public int getNumArgs() {
		
		return numArgs;
		
	}
	
	public void addArgument(Argument arg, int index) {};
	
}
