package Jtabwb.parser;

public class FunctionArgument implements Argument {
	
	private Function function;
	private Argument[] args;

	public FunctionArgument(Function function, int numArgs) {
		
		this.function = function;
		args = new Argument[numArgs];
		
	}
	
	public void addArgument(Argument arg, int index) {
		
		args[index] = arg;
		
	}
	
    public String toString() {
		
		String result = function.getName();
		result += "(";
		
		for(int i = 0; i < args.length; i++) {
			
			result += args[i].toString();
			
			if(i + 1 != args.length)
				result += ",";
			
		}
		
		result += ")";
		
		return result;
		
	}
	
	public boolean isFunction() {
		
		return true;
		
	}
	
	public boolean isVariable() {
		
		return false;
		
	}
	
}
