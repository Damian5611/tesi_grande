package Jtabwb.parser;

public class Constant implements Argument {
	
   private String name;
	
	public Constant(String name) {
		
		this.name = name; 
		
	}
	
   public String toString() {
		
		return name;
		
	}
	
	public boolean isFunction() {
		
		return false;
		
	}
	
	public boolean isVariable() {
		
		return false;
		
	}
	
	public void addArgument(Argument arg, int index) {};

}
