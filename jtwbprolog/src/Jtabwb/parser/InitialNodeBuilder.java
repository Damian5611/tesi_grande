package Jtabwb.parser;

import java.util.HashMap;
import java.util.LinkedList;

import jtwbprolog.parser.TempArgument;
import jtwbprolog.parser.TempClause;
import jtwbprolog.parser.TempGoal;
import jtwbprolog.parser.TempLiteral;
import jtwbprolog.parser.TempProgram;

public class InitialNodeBuilder {
	
	private HashMap<String, Predicate> predicateTable = new HashMap<String, Predicate>();
	private HashMap<String, Argument> argumentTable = new HashMap<String, Argument>();
	
	public Program buildPrgogram(TempProgram tp) {
		
		Program p = new Program(buildGoal(tp.getGoal()), tp.getLines());
		
		LinkedList<TempClause> clauses = tp.getClauses();
		
		int i = 0;
		for(TempClause tc : clauses)
			p.addClause(buildClause(tc), i++);
		
		return p;
		
	}

	private Goal buildGoal(TempGoal tg) {
		
		LinkedList<TempLiteral> literals = tg.getPremises();
		
		Goal goal = new Goal(literals.size());
		
		int i = 0;
		for(TempLiteral tl : literals)
			goal.addLiteral(buildLiteral(tl), i++);
		
		return goal;
		
		
	}
	
	private Clause buildClause(TempClause tc) {
		
		TempLiteral tempConclusion = tc.getConclusion();
		Literal conclusion = buildLiteral(tempConclusion);
		
		if(tc.isUnit())
			return new UnitClause(conclusion);
		
		LinkedList<TempLiteral> premises = tc.getPremises();
		
		DefiniteClause dc = new DefiniteClause(conclusion, premises.size());
			
		int i = 0;
		for(TempLiteral tl : premises)
			dc.addPremise(buildLiteral(tl), i++);
						
		return dc;
		
	}

	private Literal buildLiteral(TempLiteral tl) {
		
		Predicate predicate = null;
		String namePredicate = tl.getPredicateName();
		
		if(predicateTable.containsKey(namePredicate))
			predicate = predicateTable.get(namePredicate);
		
		else {
			
			predicate = new Predicate(namePredicate, tl.getNumArgs());
			predicateTable.put(namePredicate, predicate);
			
		}
		
		Literal literal = new Literal(predicate, predicate.getNumArgs());
		
		LinkedList<TempArgument> args  = tl.getArgs();
		
		int i = 0;
		for(TempArgument ta : args)
			literal.addArg(buildArg(ta), i++);
		
		return literal;
			
	}
	
	private Argument buildArg(TempArgument ta) {
		
		Argument arg = null;
		String argName = ta.getName();
		
		if(!ta.isFunction()) {
			
			if(argumentTable.containsKey(argName)) {
				
				arg = argumentTable.get(argName);
				return arg;
			
			}
			
			if(ta.isVariable())
				arg = new Variable(argName);
			
			else
				arg = new Constant(argName);
			
			argumentTable.put(argName, arg);
			return arg;
			
		}
		
		Function fun = null;
		LinkedList<TempArgument> args = ta.getArgs();
		
		if(argumentTable.containsKey(argName))
			fun = (Function)argumentTable.get(argName);
		
		else {
			
			fun = new Function(argName, args.size());
			argumentTable.put(argName, fun);
		}
			
		
		arg = new FunctionArgument(fun, fun.getNumArgs());
		
		int i = 0;
		for(TempArgument t : args)
			arg.addArgument(buildArg(t), i++);
		
		return arg;
		
	}
}
