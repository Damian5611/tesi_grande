package Jtabwb.parser;

public class UnitClause implements Clause {

	private Literal conclusion;
	
	public UnitClause(Literal literal) {
		
		conclusion = literal;
		
	}
	
	public boolean isUnit() {
		
		return true;
		
	}
	
	public String toString() {
		
		return conclusion.toString() + ".";
		
	}
	
	
}
