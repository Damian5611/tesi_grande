package Jtabwb.parser;

public interface Argument {
	
	public boolean isFunction();
	
	public boolean isVariable();
	
	public void addArgument(Argument arg, int index);

}
