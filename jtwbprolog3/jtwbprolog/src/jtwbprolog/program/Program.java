package jtwbprolog.program;

public class Program {

	private Clause[] clauses;
	private Goal goal;
	
	public Program(Goal goal, int numClauses) {
		
		this.goal = goal;
		clauses = new Clause[numClauses];
		
	}
	
	public void addClause(Clause clause, int index) {
		
		clauses[index] = clause;
		
	}
	
	public Clause[] getClauses() {
		
		return clauses;
	}
	
   public String toString() {
		
        String result = "";
		
		for(int i = 0; i < clauses.length; i++) {
			
			result +=i+1 + " " + clauses[i].toString();
			
			if(i + 1 != clauses.length)
				result += "\n";
			
		}
		
		result += "\n" + (clauses.length + 1)  + goal.toString();
		
		return result;
		
	}
	
}
