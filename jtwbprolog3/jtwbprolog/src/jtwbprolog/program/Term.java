package jtwbprolog.program;

public interface Term {
	
	public boolean isFunction();
	
	public boolean isVariable();
	
	public void addArgument(Term arg, int index);
	
	public Function getFunction();
	
	public Term[] getArgs();

}
