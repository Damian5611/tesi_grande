package jtwbprolog.program;

public class Variable implements Term {
	
	private String name;
	
	public Variable(String name) {
		
		this.name = name; 
		
	}
	
	public String toString() {
		
		return name;
		
	}
	
	public boolean isFunction() {
		
		return false;
		
	}
	
	public boolean isVariable() {
		
		return true;
		
	}
	
	public Function getFunction() {
		
		return null;
		
	}
	
	public Term[] getArgs() {
		
		return null;
		
	}
	
	public void addArgument(Term arg, int index) {};

}
