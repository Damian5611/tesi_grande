package jtwbprolog.program;

public class Goal {

	Literal[] literals;
	
	public Goal(int numLiterals) {
		
		literals = new Literal[numLiterals];
		
	}
	
	public void addLiteral(Literal literal, int index) {
		
		literals[index] = literal;
		
	}
	
	public Literal[] getLiterals() {
		
		return literals;
		
	}
	
	 public String toString() {
			
	    	String result = "";
	    	
	        for(int i = 0; i < literals.length; i++) {
				
				result += literals[i].toString();
				
				if(i + 1 !=  literals.length)
					result += ",";
				
			}
	    	
			return "?" + result + ".";
			
		}
	 
}
