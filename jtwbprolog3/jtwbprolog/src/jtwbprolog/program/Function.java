package jtwbprolog.program;

public class Function implements Term{

	private String name;
	private int numArgs;
	
	public Function(String name, int numArgs) {
		
		this.name = name;
		this.numArgs = numArgs;
		
	}
	
	public boolean isFunction() {
		
		return false;
		
	}
	
	public boolean isVariable() {
		
		return false;
			
	}

	public String getName() {
		
		return name;
		
	}
	
	public int getNumArgs() {
		
		return numArgs;
		
	}
	
	public Function getFunction() {
		
		return this;
		
	}
	
	public Term[] getArgs() {
		
		return null;
		
	}
	
	public void addArgument(Term arg, int index) {};
	
}
