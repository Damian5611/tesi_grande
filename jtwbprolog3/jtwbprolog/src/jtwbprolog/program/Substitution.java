package jtwbprolog.program;

import java.util.LinkedList;

public class Substitution {

	Variable[] left;
	Term[] right;
	
	public Substitution(Variable[] left, Term[] right) {
		
		this.left = left;
		this.right = right;
		
	}
	
	public static Substitution makeMGU(Literal l1, Literal l2) {
		
		LinkedList<Term> left = new LinkedList<Term>();
		LinkedList<Term> right = new LinkedList<Term>();
		
		LinkedList<Term> lresult = new LinkedList<Term>();
		LinkedList<Term> rresult = new LinkedList<Term>();
		
		for(Term t : l1.getArgs())
			left.add(t);
		
		for(Term t : l2.getArgs())
			right.add(t);
		

		boolean stop = false;
		
		do {
			
			stop = true;
			
			for(int i = 0; i < left.size(); i++) {
				
				Term lterm = left.get(i);
				Term rterm = right.get(i);
				
				if(lterm.isFunction() && rterm.isFunction()) {
					
					if(lterm.getFunction() == rterm.getFunction()) {
						
						left.remove(i);
						right.remove(i);
						
						for(Term t : lterm.getArgs())
							left.add(t);
						
						for(Term t : rterm.getArgs())
							right.add(t);
						
						stop = false;
						
					}
					
					else 
						return null;
						
				}
				
				else if(lterm == rterm) {
					
					left.remove(i);
					right.remove(i);
					
					stop = false;
					
					
				}
				
				else if(!lterm.isVariable()) {
					
					if(!rterm.isVariable())
						return null;
					
					else {
						
						left.remove(i);
						right.remove(i);
						
						left.add(rterm);
						right.add(lterm);
						
						stop = false;
						
					}
					
				}
				
				else {
					
					for(int j = 0; j < left.size(); j++) {
						
						if(j != i) {
							
							left.set(j, Substitution.substitute((Variable)lterm, rterm, left.get(j)));
							right.set(j, Substitution.substitute((Variable)lterm, rterm, right.get(j)));
							
						}
							
					}
					
                   for(int j = 0; j < lresult.size(); j++) {
						
							lresult.set(j, Substitution.substitute((Variable)lterm, rterm, lresult.get(j)));
							rresult.set(j, Substitution.substitute((Variable)lterm, rterm, rresult.get(j)));
							
					}
                   
                   left.remove(i);
                   right.remove(i);
                   
                   lresult.add(lterm);
                   rresult.add(rterm);
                   
                   stop = false;
					  
				}
				
			}
			
		} while(!stop);
		
		Variable[] l = new Variable[lresult.size()];
		Term[] r = new Term[lresult.size()];
		
		for(int k = 0; k < l.length; k++) {
			
			l[k] = (Variable)lresult.get(k);
			r[k] = rresult.get(k);
			
		}
		
		return new Substitution(l, r);
		
	}
	
	private static Term substitute(Variable v, Term c, Term t) {
		
		if(t.isFunction()) {
			
			Term[] args = new Term[t.getArgs().length];
			
			for(int i = 0; i < args.length; i++)
				args[i] = substitute(v, c, t.getArgs()[i]);
			
			return new FunctionArgument(t.getFunction(), args);
			
		}
		
		if(t.isVariable()) {
			
			if(t == v)
				return c;
			
			return t;
			
		}
		
		return t;
		
	}
	
	public void apply(Literal l) {
		
		Term[] args = l.getArgs();
		
		for(int i = 0; i < left.length; i++)
			for(int j = 0; j < args.length; j++)
				args[j] = substitute(left[i], right[i], args[j]);
			
		
	}
	
	
}
