package jtwbprolog.program;

public class Literal {

	Predicate predicate;
	Term[] args;
	
	public Literal(Predicate predicate, int numArgs) {
		
		this.predicate = predicate;
		args = new Term[numArgs];
		
	}
	
	public Term[] getArgs() {
		
		return args;
		
	}
	
	public void addArg(Term arg, int index) {
		
		args[index] = arg;
		
	}
	
   public String toString() {
		
		String result = predicate.getName();
		result += "(";
		
		for(int i = 0; i < args.length; i++) {
			
			result += args[i].toString();
			
			if(i + 1 != args.length)
				result += ",";
			
		}
		
		result += ")";
		
		return result;
		
	}
	
}
