package jtwbprolog.program;

import java.io.File;
import java.io.FileInputStream;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import jtwbprolog.parser.BuildProgram;
import jtwbprolog.parser.ErrorListener;
import jtwbprolog.parser.PrintCode;
import jtwbprolog.parser.ProLogLexer;
import jtwbprolog.parser.ProLogParser;
import jtwbprolog.parser.TempProgram;

@SuppressWarnings("deprecation")
public class Test {

public static void main(String[] args) throws Exception {
    	
    	File f = null;
    	
    	if(args.length == 0)
    		System.out.println("Bisogna inserire il percorso di un file");
    	
    	else {
    		
    		f = new File(args[0]);
    	
    	    if(!f.exists() || f.isDirectory())
    		     System.out.println("Non esiste quel file");
    	
    	     else {
    		
            
		          ANTLRInputStream input = new ANTLRInputStream(new FileInputStream(f));

                  ProLogLexer lexer = new ProLogLexer(input);

                  CommonTokenStream tokens = new CommonTokenStream(lexer);
        
                 ProLogParser parser = new ProLogParser(tokens);
                 
                 ErrorListener el = new ErrorListener();
                 
                 parser.addErrorListener(el);
                
                 ParseTree tree = parser.startingRule(); 
        
                 ParseTreeWalker walker = new ParseTreeWalker();
        
                 PrintCode p = new PrintCode();
        
                 walker.walk(p, tree);
        
                 if(!el.isWrong())
                     p.printResult();
                 
                 
                 if(!p.isWrong() && !el.isWrong()) {
                 
                    BuildProgram bp = new BuildProgram();
                 
                    walker.walk(bp, tree);
                 
                    TempProgram pro = bp.getProgram();
                 
                    System.out.println(pro.toString());
                 
                    System.out.println();
                 
                    bp.printError();
                    
                    if(!bp.getError()) {
                    	
                    	Program program = (new InitialNodeBuilder()).buildPrgogram(pro);
                    	System.out.println("\n\n" + program.toString() + "\n\n");
                    	
                    	Clause[] c = program.getClauses();
                    	
                    	Literal l1 = ((UnitClause)c[0]).getLiteral();
                    	Literal l2 = ((UnitClause)c[1]).getLiteral();
                    	
                    	Substitution s = Substitution.makeMGU(l1, l2);
                    	
                    	if(s == null)
                    		System.out.println("Letterali non unificabili");
                    	
                    	else {
                    		
                    		System.out.println(l1.toString());
                    		System.out.println(l2.toString() + "\n\n");
                    		
                    		
                    		s.apply(l1);
                    		s.apply(l2);
                    		
                    		System.out.println(l1.toString());
                    		System.out.println(l2.toString());
                    		
                    	}
                    	
                    }
                 
                 }
        
    	}
    	
      }
    	
    }
	
}
