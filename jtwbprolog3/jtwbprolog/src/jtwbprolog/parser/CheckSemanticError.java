package jtwbprolog.parser;

import java.util.HashMap;

public class CheckSemanticError {

	int errorIndex = 0;
	
	public boolean checkPredicateOrFunction(HashMap<String, Integer> map1, HashMap<String, Integer> map2, toCheck tc) {
		
		if(map1.containsKey(tc.getName())) {
		
			if(tc.getType() == toCheck.Type.LITERAL)
				errorIndex = 0;
			
			else
				errorIndex = 2;
			
			return true;
		
		}
		
		
		else if(map2.containsKey(tc.getName()) && (map2.get(tc.getName()) != tc.getNumArgs())) {
			
			if(tc.getType() == toCheck.Type.LITERAL)
				errorIndex = 1;
			
			else
				errorIndex = 3;
			
			return true;
		}
		
		return false;
		
	}
	
	public void updateTable(HashMap<String, Integer> map, toCheck tc) {
		
		 if (!map.containsKey(tc.getName())) {
				
				map.put(tc.getName(), tc.getNumArgs());
		
	      }
		 
	}
	
	public int getErrorIndex() {
		
		return errorIndex;
		
	}
	
}
