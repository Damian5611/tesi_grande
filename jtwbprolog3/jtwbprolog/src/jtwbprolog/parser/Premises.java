package jtwbprolog.parser;

import java.util.LinkedList;

public class Premises {

	private LinkedList<TempLiteral> literals = new LinkedList<TempLiteral>();
	
	public void addLiteral(TempLiteral literal) {
		
		literals.add(literal);
		
	}
	
    public LinkedList<TempLiteral> getPremises() {
		
		return literals;
		
	}
	
    public String toString() {
		
		String result = "";
		
		for(int i = 0; i < literals.size(); i++) {
			
			result += literals.get(i).toString();
			
			if(i + 1 != literals.size())
				result += ",";
			
		}
		
		return result;
		
	}
	
}
