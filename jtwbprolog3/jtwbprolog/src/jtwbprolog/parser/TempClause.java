package jtwbprolog.parser;

import java.util.LinkedList;

abstract public class TempClause {
	
	protected Conclusion conclusion;
	
	public void setConclusion(Conclusion conclusion) {
		
		this.conclusion = conclusion;
		
	}
	
	public TempLiteral getConclusion() {
		
		return conclusion.getConclusion();
		
	}
	
	public abstract boolean isUnit();
		
    public abstract LinkedList<TempLiteral> getPremises();
		
	public void setPremises(Premises premises) { };
	
	public abstract String toString();

}
