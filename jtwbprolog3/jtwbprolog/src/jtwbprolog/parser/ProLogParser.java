// Generated from ProLog.g4 by ANTLR 4.4

  package jtwbprolog.parser; 

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ProLogParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.4", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__5=1, T__4=2, T__3=3, T__2=4, T__1=5, T__0=6, LOWERCASE_ID=7, UPPERCASE_ID=8, 
		WS=9;
	public static final String[] tokenNames = {
		"<INVALID>", "'?'", "':-'", "'('", "')'", "','", "'.'", "LOWERCASE_ID", 
		"UPPERCASE_ID", "WS"
	};
	public static final int
		RULE_startingRule = 0, RULE_clause = 1, RULE_definiteClause = 2, RULE_unitClause = 3, 
		RULE_conclusion = 4, RULE_premises = 5, RULE_literal = 6, RULE_functionOrConstant = 7, 
		RULE_argument = 8, RULE_functionName = 9, RULE_predicate = 10, RULE_variable = 11, 
		RULE_goal = 12;
	public static final String[] ruleNames = {
		"startingRule", "clause", "definiteClause", "unitClause", "conclusion", 
		"premises", "literal", "functionOrConstant", "argument", "functionName", 
		"predicate", "variable", "goal"
	};

	@Override
	public String getGrammarFileName() { return "ProLog.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public ProLogParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class StartingRuleContext extends ParserRuleContext {
		public GoalContext goal() {
			return getRuleContext(GoalContext.class,0);
		}
		public TerminalNode EOF() { return getToken(ProLogParser.EOF, 0); }
		public ClauseContext clause(int i) {
			return getRuleContext(ClauseContext.class,i);
		}
		public List<ClauseContext> clause() {
			return getRuleContexts(ClauseContext.class);
		}
		public StartingRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_startingRule; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProLogListener ) ((ProLogListener)listener).enterStartingRule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProLogListener ) ((ProLogListener)listener).exitStartingRule(this);
		}
	}

	public final StartingRuleContext startingRule() throws RecognitionException {
		StartingRuleContext _localctx = new StartingRuleContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_startingRule);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(27); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(26); clause();
				}
				}
				setState(29); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==LOWERCASE_ID );
			setState(31); goal();
			setState(32); match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClauseContext extends ParserRuleContext {
		public DefiniteClauseContext definiteClause() {
			return getRuleContext(DefiniteClauseContext.class,0);
		}
		public UnitClauseContext unitClause() {
			return getRuleContext(UnitClauseContext.class,0);
		}
		public ClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_clause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProLogListener ) ((ProLogListener)listener).enterClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProLogListener ) ((ProLogListener)listener).exitClause(this);
		}
	}

	public final ClauseContext clause() throws RecognitionException {
		ClauseContext _localctx = new ClauseContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_clause);
		try {
			setState(36);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(34); definiteClause();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(35); unitClause();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DefiniteClauseContext extends ParserRuleContext {
		public PremisesContext premises() {
			return getRuleContext(PremisesContext.class,0);
		}
		public ConclusionContext conclusion() {
			return getRuleContext(ConclusionContext.class,0);
		}
		public DefiniteClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_definiteClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProLogListener ) ((ProLogListener)listener).enterDefiniteClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProLogListener ) ((ProLogListener)listener).exitDefiniteClause(this);
		}
	}

	public final DefiniteClauseContext definiteClause() throws RecognitionException {
		DefiniteClauseContext _localctx = new DefiniteClauseContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_definiteClause);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(38); conclusion();
			setState(39); match(T__4);
			setState(40); premises();
			setState(41); match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UnitClauseContext extends ParserRuleContext {
		public ConclusionContext conclusion() {
			return getRuleContext(ConclusionContext.class,0);
		}
		public UnitClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unitClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProLogListener ) ((ProLogListener)listener).enterUnitClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProLogListener ) ((ProLogListener)listener).exitUnitClause(this);
		}
	}

	public final UnitClauseContext unitClause() throws RecognitionException {
		UnitClauseContext _localctx = new UnitClauseContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_unitClause);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(43); conclusion();
			setState(44); match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConclusionContext extends ParserRuleContext {
		public LiteralContext literal() {
			return getRuleContext(LiteralContext.class,0);
		}
		public ConclusionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_conclusion; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProLogListener ) ((ProLogListener)listener).enterConclusion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProLogListener ) ((ProLogListener)listener).exitConclusion(this);
		}
	}

	public final ConclusionContext conclusion() throws RecognitionException {
		ConclusionContext _localctx = new ConclusionContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_conclusion);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(46); literal();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PremisesContext extends ParserRuleContext {
		public LiteralContext literal(int i) {
			return getRuleContext(LiteralContext.class,i);
		}
		public List<LiteralContext> literal() {
			return getRuleContexts(LiteralContext.class);
		}
		public PremisesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_premises; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProLogListener ) ((ProLogListener)listener).enterPremises(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProLogListener ) ((ProLogListener)listener).exitPremises(this);
		}
	}

	public final PremisesContext premises() throws RecognitionException {
		PremisesContext _localctx = new PremisesContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_premises);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(48); literal();
			setState(53);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__1) {
				{
				{
				setState(49); match(T__1);
				setState(50); literal();
				}
				}
				setState(55);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LiteralContext extends ParserRuleContext {
		public List<ArgumentContext> argument() {
			return getRuleContexts(ArgumentContext.class);
		}
		public ArgumentContext argument(int i) {
			return getRuleContext(ArgumentContext.class,i);
		}
		public PredicateContext predicate() {
			return getRuleContext(PredicateContext.class,0);
		}
		public LiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProLogListener ) ((ProLogListener)listener).enterLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProLogListener ) ((ProLogListener)listener).exitLiteral(this);
		}
	}

	public final LiteralContext literal() throws RecognitionException {
		LiteralContext _localctx = new LiteralContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_literal);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(56); predicate();
			setState(57); match(T__3);
			setState(58); argument();
			setState(63);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__1) {
				{
				{
				setState(59); match(T__1);
				setState(60); argument();
				}
				}
				setState(65);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(66); match(T__2);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionOrConstantContext extends ParserRuleContext {
		public FunctionNameContext functionName() {
			return getRuleContext(FunctionNameContext.class,0);
		}
		public List<ArgumentContext> argument() {
			return getRuleContexts(ArgumentContext.class);
		}
		public ArgumentContext argument(int i) {
			return getRuleContext(ArgumentContext.class,i);
		}
		public FunctionOrConstantContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionOrConstant; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProLogListener ) ((ProLogListener)listener).enterFunctionOrConstant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProLogListener ) ((ProLogListener)listener).exitFunctionOrConstant(this);
		}
	}

	public final FunctionOrConstantContext functionOrConstant() throws RecognitionException {
		FunctionOrConstantContext _localctx = new FunctionOrConstantContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_functionOrConstant);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(68); functionName();
			setState(81);
			_la = _input.LA(1);
			if (_la==T__3) {
				{
				setState(69); match(T__3);
				setState(78);
				_la = _input.LA(1);
				if (_la==LOWERCASE_ID || _la==UPPERCASE_ID) {
					{
					setState(70); argument();
					setState(75);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==T__1) {
						{
						{
						setState(71); match(T__1);
						setState(72); argument();
						}
						}
						setState(77);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(80); match(T__2);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgumentContext extends ParserRuleContext {
		public FunctionOrConstantContext functionOrConstant() {
			return getRuleContext(FunctionOrConstantContext.class,0);
		}
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public ArgumentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_argument; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProLogListener ) ((ProLogListener)listener).enterArgument(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProLogListener ) ((ProLogListener)listener).exitArgument(this);
		}
	}

	public final ArgumentContext argument() throws RecognitionException {
		ArgumentContext _localctx = new ArgumentContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_argument);
		try {
			setState(85);
			switch (_input.LA(1)) {
			case UPPERCASE_ID:
				enterOuterAlt(_localctx, 1);
				{
				setState(83); variable();
				}
				break;
			case LOWERCASE_ID:
				enterOuterAlt(_localctx, 2);
				{
				setState(84); functionOrConstant();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionNameContext extends ParserRuleContext {
		public TerminalNode LOWERCASE_ID() { return getToken(ProLogParser.LOWERCASE_ID, 0); }
		public FunctionNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProLogListener ) ((ProLogListener)listener).enterFunctionName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProLogListener ) ((ProLogListener)listener).exitFunctionName(this);
		}
	}

	public final FunctionNameContext functionName() throws RecognitionException {
		FunctionNameContext _localctx = new FunctionNameContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_functionName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(87); match(LOWERCASE_ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PredicateContext extends ParserRuleContext {
		public TerminalNode LOWERCASE_ID() { return getToken(ProLogParser.LOWERCASE_ID, 0); }
		public PredicateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_predicate; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProLogListener ) ((ProLogListener)listener).enterPredicate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProLogListener ) ((ProLogListener)listener).exitPredicate(this);
		}
	}

	public final PredicateContext predicate() throws RecognitionException {
		PredicateContext _localctx = new PredicateContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_predicate);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(89); match(LOWERCASE_ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableContext extends ParserRuleContext {
		public TerminalNode UPPERCASE_ID() { return getToken(ProLogParser.UPPERCASE_ID, 0); }
		public VariableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProLogListener ) ((ProLogListener)listener).enterVariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProLogListener ) ((ProLogListener)listener).exitVariable(this);
		}
	}

	public final VariableContext variable() throws RecognitionException {
		VariableContext _localctx = new VariableContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_variable);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(91); match(UPPERCASE_ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GoalContext extends ParserRuleContext {
		public PremisesContext premises() {
			return getRuleContext(PremisesContext.class,0);
		}
		public GoalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_goal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProLogListener ) ((ProLogListener)listener).enterGoal(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProLogListener ) ((ProLogListener)listener).exitGoal(this);
		}
	}

	public final GoalContext goal() throws RecognitionException {
		GoalContext _localctx = new GoalContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_goal);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(93); match(T__5);
			setState(94); premises();
			setState(95); match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\13d\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4"+
		"\f\t\f\4\r\t\r\4\16\t\16\3\2\6\2\36\n\2\r\2\16\2\37\3\2\3\2\3\2\3\3\3"+
		"\3\5\3\'\n\3\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\6\3\6\3\7\3\7\3\7\7\7\66"+
		"\n\7\f\7\16\79\13\7\3\b\3\b\3\b\3\b\3\b\7\b@\n\b\f\b\16\bC\13\b\3\b\3"+
		"\b\3\t\3\t\3\t\3\t\3\t\7\tL\n\t\f\t\16\tO\13\t\5\tQ\n\t\3\t\5\tT\n\t\3"+
		"\n\3\n\5\nX\n\n\3\13\3\13\3\f\3\f\3\r\3\r\3\16\3\16\3\16\3\16\3\16\2\2"+
		"\17\2\4\6\b\n\f\16\20\22\24\26\30\32\2\2^\2\35\3\2\2\2\4&\3\2\2\2\6(\3"+
		"\2\2\2\b-\3\2\2\2\n\60\3\2\2\2\f\62\3\2\2\2\16:\3\2\2\2\20F\3\2\2\2\22"+
		"W\3\2\2\2\24Y\3\2\2\2\26[\3\2\2\2\30]\3\2\2\2\32_\3\2\2\2\34\36\5\4\3"+
		"\2\35\34\3\2\2\2\36\37\3\2\2\2\37\35\3\2\2\2\37 \3\2\2\2 !\3\2\2\2!\""+
		"\5\32\16\2\"#\7\2\2\3#\3\3\2\2\2$\'\5\6\4\2%\'\5\b\5\2&$\3\2\2\2&%\3\2"+
		"\2\2\'\5\3\2\2\2()\5\n\6\2)*\7\4\2\2*+\5\f\7\2+,\7\b\2\2,\7\3\2\2\2-."+
		"\5\n\6\2./\7\b\2\2/\t\3\2\2\2\60\61\5\16\b\2\61\13\3\2\2\2\62\67\5\16"+
		"\b\2\63\64\7\7\2\2\64\66\5\16\b\2\65\63\3\2\2\2\669\3\2\2\2\67\65\3\2"+
		"\2\2\678\3\2\2\28\r\3\2\2\29\67\3\2\2\2:;\5\26\f\2;<\7\5\2\2<A\5\22\n"+
		"\2=>\7\7\2\2>@\5\22\n\2?=\3\2\2\2@C\3\2\2\2A?\3\2\2\2AB\3\2\2\2BD\3\2"+
		"\2\2CA\3\2\2\2DE\7\6\2\2E\17\3\2\2\2FS\5\24\13\2GP\7\5\2\2HM\5\22\n\2"+
		"IJ\7\7\2\2JL\5\22\n\2KI\3\2\2\2LO\3\2\2\2MK\3\2\2\2MN\3\2\2\2NQ\3\2\2"+
		"\2OM\3\2\2\2PH\3\2\2\2PQ\3\2\2\2QR\3\2\2\2RT\7\6\2\2SG\3\2\2\2ST\3\2\2"+
		"\2T\21\3\2\2\2UX\5\30\r\2VX\5\20\t\2WU\3\2\2\2WV\3\2\2\2X\23\3\2\2\2Y"+
		"Z\7\t\2\2Z\25\3\2\2\2[\\\7\t\2\2\\\27\3\2\2\2]^\7\n\2\2^\31\3\2\2\2_`"+
		"\7\3\2\2`a\5\f\7\2ab\7\b\2\2b\33\3\2\2\2\n\37&\67AMPSW";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}