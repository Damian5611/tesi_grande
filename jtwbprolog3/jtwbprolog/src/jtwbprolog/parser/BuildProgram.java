package jtwbprolog.parser;

import java.util.HashMap;
import java.util.Stack;

import org.antlr.v4.runtime.tree.TerminalNode;

public class BuildProgram extends ProLogBaseListener {
	
	private TempProgram program;
	private TempClause clause;
	private TempGoal goal;
	private Conclusion conclusion;
	private Premises premises;
	private TempLiteral literal;
	private TempPredicate predicate;
	private TempVariable variable;
	private Stack<TempFunction> function = new Stack<TempFunction>();
	
	private HashMap<String, Integer> predicateTable = new HashMap<String, Integer>();
	private HashMap<String, Integer> functionTable = new HashMap<String, Integer>();
	
	private boolean error = false;
	
	private int numLines;
	private String namePredicateOrFunction;
	
	private String error1 = "Error at line ";
	private String error2 =  ": the name \"";
	
	private String[] error3 = {
			
			"\" is already used for a function", //0
			"\" is already used for a predicate with a different number of arguments", //1
			"\" is already used for a predicate", //2
		    "\" is already used for a function with a differnet number of arguments" //3
			
	};
	
	private CheckSemanticError cse;
	int aggiungiLinea = 0;
	
	int toBeNamed = 10;
	
	public TempProgram getProgram() {
		
		return program;
		
	}
	
	public boolean getError() {
		
		return error;
		
	}
	
	public void printError() {
		
		if(error)
			System.out.println(error1 + numLines + error2 + namePredicateOrFunction + error3[cse.getErrorIndex()]);
		
	}
	
	public void enterStartingRule(ProLogParser.StartingRuleContext ctx) { 
		
		program = new TempProgram();
		cse = new CheckSemanticError();
		
	}
	
	public void enterUnitClause(ProLogParser.UnitClauseContext ctx) { 
		
		clause = new TempUnitClause();
		program.addClause(clause);
		
	}
	
	public void enterGoal(ProLogParser.GoalContext ctx) {
		
		goal = new TempGoal();
		aggiungiLinea = 1;
		program.setGoal(goal);
		
	}
	
	public void enterDefiniteClause(ProLogParser.DefiniteClauseContext ctx) { 
		
		clause = new TempDefiniteClause();
		program.addClause(clause);
		
	}
	
	public void enterConclusion(ProLogParser.ConclusionContext ctx) { 
		
		conclusion = new Conclusion();
		clause.setConclusion(conclusion);
		
	}
	
	public void enterPremises(ProLogParser.PremisesContext ctx) { 
	
		premises = new Premises();
		
		if(ctx.getParent().getRuleIndex() == ProLogParser.RULE_definiteClause)
			clause.setPremises(premises);
		
		else
			goal.setPremises(premises);
		
	}
	
	public void enterLiteral(ProLogParser.LiteralContext ctx) { 
		
		literal = new TempLiteral();
		
		if(ctx.getParent().getRuleIndex() == ProLogParser.RULE_conclusion)
			conclusion.setLiteral(literal);
		
		else
			premises.addLiteral(literal);
		
	}
	
	 public void enterPredicate(ProLogParser.PredicateContext ctx) { 
		 
		 predicate = new TempPredicate();
		 literal.setPredicate(predicate);
		 
		 toBeNamed = 0;
		 
	 }
	 
	 public void exitLiteral(ProLogParser.LiteralContext ctx) { 
		
		 if(!error && cse.checkPredicateOrFunction(functionTable, predicateTable, literal)) {
			 
			 error = true;
			 namePredicateOrFunction = literal.getName();
			 numLines = program.getLines() + aggiungiLinea;
			
		 }
		 
		 else {
			 
		     cse.updateTable(predicateTable, literal);
		     
		 }
		 
	 }
	 
	public  void enterVariable(ProLogParser.VariableContext ctx) {
		
		variable = new TempVariable();
		
		if(ctx.getParent().getParent().getRuleIndex() == ProLogParser.RULE_literal)
			literal.addArgument(variable);
		
		else
			function.peek().addArgument(variable);
			
		 toBeNamed = 2; 
		
	 }
	
	public void enterFunctionOrConstant(ProLogParser.FunctionOrConstantContext ctx) {
		
		TempFunction fun = new TempFunction();
		
		if(ctx.getParent().getParent().getRuleIndex() == ProLogParser.RULE_literal)
			literal.addArgument(fun);
		
		else
			function.peek().addArgument(fun);
		
		function.push(fun);
		
	}
	 
	public void exitFunctionOrConstant(ProLogParser.FunctionOrConstantContext ctx) {
		
		TempFunction fun = function.pop();
		
		if(!fun.isFunction()) {
			
			if(ctx.getParent().getParent().getRuleIndex() == ProLogParser.RULE_literal)
				literal.changeToCostant();
			
			else
				function.peek().changeToCostant();
			
		}
		
		
         if(!error && cse.checkPredicateOrFunction(predicateTable, functionTable, fun)) {
			 
			 error = true;
			 namePredicateOrFunction = fun.getName();
			 numLines = program.getLines() + aggiungiLinea;
			 
		 }
         
         else {
        	 
		    cse.updateTable(functionTable, fun);
		    
         }
         
	}
	
	public void enterFunctionName(ProLogParser.FunctionNameContext ctx) { 
		
		toBeNamed = 1;
		
	}
	
	public void visitTerminal(TerminalNode node) {
		
		if(toBeNamed == 0)
			predicate.setName(node.getText());
		
		else if(toBeNamed == 1)
			function.peek().setName(node.getText());
		
		else if(toBeNamed == 2)
			variable.setName(node.getText());
		
		toBeNamed = 10;
		
	}
	
	
}
