grammar ProLog;

@header{
  package jtwbprolog.parser; 
}

startingRule: clause+ goal EOF;

clause:   definiteClause 
      |   unitClause 
      ;


definiteClause:   conclusion  ':-' premises '.';

unitClause:   conclusion '.';

conclusion: literal;

premises: literal (',' literal)*;

literal:   predicate '(' argument (',' argument)* ')';

functionOrConstant: functionName ( '(' (argument (',' argument)*)? ')' )?; 

argument:   variable
        |   functionOrConstant 
        ;

functionName: LOWERCASE_ID;

predicate: LOWERCASE_ID;

variable: UPPERCASE_ID;

goal: '?' premises '.';

LOWERCASE_ID: 'a'..'z'('a'..'z' | 'A'..'Z' | '0'..'9')*;

UPPERCASE_ID: 'A'..'Z'('a'..'z' | 'A'..'Z' | '0'..'9')*;

WS : [ \t\r\n]+ -> skip;