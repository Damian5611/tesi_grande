package jtwbprolog.parser;

import java.io.File;
import java.io.FileInputStream;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import jtwbprolog.program.InitialNodeBuilder;
import jtwbprolog.program.Program;

@SuppressWarnings("deprecation")
public class Test {

    public static void main(String[] args) throws Exception {
    	
    	File f = null;
    	
    	if(args.length == 0)
    		System.out.println("Bisogna inserire il percorso di un file");
    	
    	else {
    		
    		f = new File(args[0]);
    	
    	    if(!f.exists() || f.isDirectory())
    		     System.out.println("Non esiste quel file");
    	
    	     else {
    		
            
		          ANTLRInputStream input = new ANTLRInputStream(new FileInputStream(f));

                  ProLogLexer lexer = new ProLogLexer(input);

                  CommonTokenStream tokens = new CommonTokenStream(lexer);
        
                 ProLogParser parser = new ProLogParser(tokens);
                 
                 ErrorListener el = new ErrorListener();
                 
                 parser.addErrorListener(el);
                
                 ParseTree tree = parser.startingRule(); 
        
                 ParseTreeWalker walker = new ParseTreeWalker();
        
                 PrintCode p = new PrintCode();
        
                 walker.walk(p, tree);
        
                 if(!el.isWrong())
                     p.printResult();
                 
                 
                 if(!p.isWrong() && !el.isWrong()) {
                 
                    BuildProgram bp = new BuildProgram();
                 
                    walker.walk(bp, tree);
                 
                    TempProgram pro = bp.getProgram();
                 
                    System.out.println(pro.toString());
                 
                    System.out.println();
                 
                    bp.printError();
                    
                    if(!bp.getError()) {
                    	
                    	Program program = (new InitialNodeBuilder()).buildPrgogram(pro);
                    	System.out.println("\n\n" + program.toString());
                    	
                    }
                 
                 }
        
    	}
    	
      }
    	
    }
}