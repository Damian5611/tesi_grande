package jtwbprolog.parser;

public class Conclusion {

	private TempLiteral literal;
	
	public void setLiteral(TempLiteral literal) {
		
		this.literal = literal;
			
	}
	
    public TempLiteral getConclusion() {
		
		return literal;
		
	}
	
	public String  toString() {
		
		return literal.toString();
		
	}
	
}
