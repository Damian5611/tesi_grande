#! /bin/bash

JTWBPROLOG_HOME="."
JTWBPROLOG_LIB="$JTWBPROLOG_HOME/lib"
JTWBPROLOG_BIN="$JTWBPROLOG_HOME/bin"

# ADDING LIBS
CP=$CP":$JTWBPROLOG_LIB/antlr-runtime-4.7.jar"
CP=$CP":$JTWBPROLOG_LIB/commons-cli-1.3.1.jar"
CP=$CP":$JTWBPROLOG_LIB/commons-logging-1.2.jar"
CP=$CP":$JTWBPROLOG_LIB/ferram-util-1.0.jar"
CP=$CP":$JTWBPROLOG_LIB/jtabwb.jar"

# ADDING PROJECT BIN
CP=$CP":$JTWBPROLOG_BIN"

java -cp "$CP" -Xss16m -server jtwbprolog.launcher.Main $*

