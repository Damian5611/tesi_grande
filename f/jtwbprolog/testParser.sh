#! /bin/bash

JTWBPROLOG_HOME="."
JTWBPROLOG_LIB="$JTWBPROLOG_HOME/lib"
JTWBPROLOG_BIN="$JTWBPROLOG_HOME/bin"

# ADDING LIBS
CP=$CP":$JTWBPROLOG_LIB/antlr-runtime-4.7.jar"

# ADDING PROJECT BIN
CP=$CP":$JTWBPROLOG_BIN"

echo "$CP"

java -cp "$CP" -Xss16m -server jtwbprolog.parser.Test $*

