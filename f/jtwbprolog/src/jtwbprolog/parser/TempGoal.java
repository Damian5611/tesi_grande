package jtwbprolog.parser;

import java.util.LinkedList;

public class TempGoal {

	private Premises premises;
	
	public LinkedList<TempLiteral> getPremises() {
		
		return premises.getPremises();
		
	}
	
	public void setPremises(Premises premises) {
		
		this.premises = premises; 
		
	}
	
	public String toString() {
		
		return "?" + premises.toString() + ".";
		
	}
	
}
