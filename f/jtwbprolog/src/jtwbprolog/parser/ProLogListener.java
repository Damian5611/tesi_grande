// Generated from ProLog.g4 by ANTLR 4.7

  package jtwbprolog.parser; 

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ProLogParser}.
 */
public interface ProLogListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ProLogParser#startingRule}.
	 * @param ctx the parse tree
	 */
	void enterStartingRule(ProLogParser.StartingRuleContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProLogParser#startingRule}.
	 * @param ctx the parse tree
	 */
	void exitStartingRule(ProLogParser.StartingRuleContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProLogParser#clause}.
	 * @param ctx the parse tree
	 */
	void enterClause(ProLogParser.ClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProLogParser#clause}.
	 * @param ctx the parse tree
	 */
	void exitClause(ProLogParser.ClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProLogParser#definiteClause}.
	 * @param ctx the parse tree
	 */
	void enterDefiniteClause(ProLogParser.DefiniteClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProLogParser#definiteClause}.
	 * @param ctx the parse tree
	 */
	void exitDefiniteClause(ProLogParser.DefiniteClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProLogParser#unitClause}.
	 * @param ctx the parse tree
	 */
	void enterUnitClause(ProLogParser.UnitClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProLogParser#unitClause}.
	 * @param ctx the parse tree
	 */
	void exitUnitClause(ProLogParser.UnitClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProLogParser#conclusion}.
	 * @param ctx the parse tree
	 */
	void enterConclusion(ProLogParser.ConclusionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProLogParser#conclusion}.
	 * @param ctx the parse tree
	 */
	void exitConclusion(ProLogParser.ConclusionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProLogParser#premises}.
	 * @param ctx the parse tree
	 */
	void enterPremises(ProLogParser.PremisesContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProLogParser#premises}.
	 * @param ctx the parse tree
	 */
	void exitPremises(ProLogParser.PremisesContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProLogParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterLiteral(ProLogParser.LiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProLogParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitLiteral(ProLogParser.LiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProLogParser#functionOrConstant}.
	 * @param ctx the parse tree
	 */
	void enterFunctionOrConstant(ProLogParser.FunctionOrConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProLogParser#functionOrConstant}.
	 * @param ctx the parse tree
	 */
	void exitFunctionOrConstant(ProLogParser.FunctionOrConstantContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProLogParser#argument}.
	 * @param ctx the parse tree
	 */
	void enterArgument(ProLogParser.ArgumentContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProLogParser#argument}.
	 * @param ctx the parse tree
	 */
	void exitArgument(ProLogParser.ArgumentContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProLogParser#functionName}.
	 * @param ctx the parse tree
	 */
	void enterFunctionName(ProLogParser.FunctionNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProLogParser#functionName}.
	 * @param ctx the parse tree
	 */
	void exitFunctionName(ProLogParser.FunctionNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProLogParser#predicate}.
	 * @param ctx the parse tree
	 */
	void enterPredicate(ProLogParser.PredicateContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProLogParser#predicate}.
	 * @param ctx the parse tree
	 */
	void exitPredicate(ProLogParser.PredicateContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProLogParser#variable}.
	 * @param ctx the parse tree
	 */
	void enterVariable(ProLogParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProLogParser#variable}.
	 * @param ctx the parse tree
	 */
	void exitVariable(ProLogParser.VariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProLogParser#goal}.
	 * @param ctx the parse tree
	 */
	void enterGoal(ProLogParser.GoalContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProLogParser#goal}.
	 * @param ctx the parse tree
	 */
	void exitGoal(ProLogParser.GoalContext ctx);
}