package jtwbprolog.parser;

import java.util.LinkedList;

public class TempLiteral implements toCheck {
	
	private TempPredicate predicate;
	private LinkedList<TempArgument> arguments = new LinkedList<TempArgument>();
 
	public Type getType() {
		
		return Type.LITERAL;
		
	}
	
	public LinkedList<TempArgument> getArgs() {
		
		return arguments;
		
	}
	
	public String getPredicateName() {
		
		return predicate.getName();
		
	}
	
	public String getName() {
		
		return predicate.getName();
		
	}
	
	public  int getNumArgs() {
		
		return arguments.size();
		
	}
	
	public void setPredicate(TempPredicate predicate) {
		
		this.predicate = predicate;
		
	}
	
	public void addArgument(TempArgument argument) {
		
		arguments.add(argument);
		
	}
	
	public void changeToCostant() {
		
		TempArgument argument = arguments.getLast();
		TempConstant constant = new TempConstant(argument.getName());
		arguments.removeLast();
		arguments.add(constant);
		
	}
	
	public String toString() {
		
		String result = predicate.toString();
		result += "(";
		
		for(int i = 0; i < arguments.size(); i++) {
			
			result += arguments.get(i).toString();
			
			if(i + 1 != arguments.size())
				result += ",";
			
		}
		
		result += ")";
		
		return result;
		
	}
	
}
