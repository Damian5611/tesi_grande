package jtwbprolog.parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import jtwbprolog.program.InitialNodeBuilder;
import jtwbprolog.program.Program;

public class JTWBPrologReader {

  public JTWBPrologReader(File inputFile) {
    super();
    this.inputFile = inputFile;
  }

  private File inputFile;

  public Program read() throws FileNotFoundException, IOException {
    ANTLRInputStream input = new ANTLRInputStream(new FileInputStream(inputFile));
    ProLogLexer lexer = new ProLogLexer(input);
    CommonTokenStream tokens = new CommonTokenStream(lexer);

    ProLogParser parser = new ProLogParser(tokens);
    ErrorListener el = new ErrorListener();
    parser.addErrorListener(el);
    ParseTree tree = parser.startingRule();
    ParseTreeWalker walker = new ParseTreeWalker();
    BuildProgram bp = new BuildProgram();
    walker.walk(bp, tree);
    TempProgram pro = bp.getProgram();
    return (new InitialNodeBuilder()).buildPrgogram(pro);
  }

}
