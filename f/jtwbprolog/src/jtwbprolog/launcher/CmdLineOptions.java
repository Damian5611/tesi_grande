package jtwbprolog.launcher;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.logging.impl.SimpleLog;

import ferram.CLIOptionsSupport.CLIOptionsSupport;
import jtwbprolog.util.MSGManager;

public class CmdLineOptions {

  public CmdLineOptions() {
    super();
    this.options = new Options();
    buildCmdLineOptions();
    this.optionSupport = new CLIOptionsSupport();
    this.setIncompatibleOptions();
  }

  private static class CmdLineOptionsNames {

    static String HELP = "h";
    static String VERBOSE = "v";
    static String LOG_DEBUG = "debug";
  }

  private void setIncompatibleOptions() {
  }

  private Options options;
  private CLIOptionsSupport optionSupport;

  private Options buildCmdLineOptions() {
    // HELP
    options.addOption(Option.builder(CmdLineOptionsNames.HELP).desc(MSG.OPTIONS.HELP).build());
    // VERBOSE OPTION
    options.addOption(Option.builder(CmdLineOptionsNames.VERBOSE).hasArg(false)
        .desc(String.format(MSG.OPTIONS.VERBOSE)).build());
    // LOG_DEBUG option
    options.addOption(Option.builder(CmdLineOptionsNames.LOG_DEBUG).hasArg(false)
        .desc(String.format(MSG.OPTIONS.LOG_DEBUG)).build());
    return options;
  }

  /**
   * Processes the command line options and returns an object describing the
   * launcher execution configuration.
   * 
   * @param args the command line arguments
   */
  public LauncherExecConfiguration processCommandLineOptions(String[] args) {
    CommandLine commandLine = null;
    try {
      CommandLineParser parser = new DefaultParser();
      // parse the command line arguments
      commandLine = parser.parse(options, args, false);

      // help optption
      if (commandLine.hasOption(CmdLineOptionsNames.HELP)) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(this.getClass().getCanonicalName(),
            MSG.OPTIONS_MANAGMENT.INFO.HELP_HEADER, options, MSG.OPTIONS_MANAGMENT.INFO.HELP_FOOTER,
            true);
        System.exit(0);
      }

      // Check options compatibility
      if (optionSupport.hasIncompatibleOptions(commandLine)) {
        MSGManager.error(optionSupport.firstIncompatibilityDescription(commandLine));
        System.exit(1);
      }

      LauncherExecConfiguration configuration = new LauncherExecConfiguration();

      // log mode
      if (commandLine.hasOption(CmdLineOptionsNames.LOG_DEBUG)) {
        configuration.setLogMode(SimpleLog.LOG_LEVEL_DEBUG);
        configuration.setExecutuionMode(LauncherExecMode.VERBOSE);
      } else
        configuration.setLogMode(SimpleLog.LOG_LEVEL_OFF);

      // verbose mode
      if (commandLine.hasOption(CmdLineOptionsNames.VERBOSE))
        configuration.setExecutuionMode(LauncherExecMode.VERBOSE);

      // extract intput file names
      configuration.setInputFileNames(commandLine.getArgs());
      return configuration;
    } catch (ParseException exp) {
      // oops, something went wrong
      MSGManager.error(MSG.OPTIONS_MANAGMENT.ERRORS.INVOCATION_ERROR, exp.getMessage());
      System.exit(1);
      return null;
    }
  }

}
