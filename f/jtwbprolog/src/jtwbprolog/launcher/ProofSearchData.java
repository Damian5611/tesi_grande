package jtwbprolog.launcher;

import jtabwb.engine.Engine;
import jtwbprolog.tp.TPProlog;

public class ProofSearchData {

  public ProofSearchData(Engine engine, TPProlog prover, long proof_search_start_time,
      long proof_search_end_time) {
    super();
    this.engine = engine;
    this.prover = prover;
    this.proof_search_start_time = proof_search_start_time;
    this.proof_search_end_time = proof_search_end_time;
  }

  private TPProlog prover;
  private Engine engine;
  private long proof_search_start_time = 0;
  private long proof_search_end_time = -1;

  /**
   * @return the prover
   */
  public TPProlog getProver() {
    return this.prover;
  }

  /**
   * @return the proof_search_start_time
   */
  public long getProof_search_start_time() {
    return this.proof_search_start_time;
  }

  /**
   * @return the proof_search_end_time
   */
  public long getProof_search_end_time() {
    return this.proof_search_end_time;
  }

  /**
   * Returns the time required by the proof-search in milliseconds.
   * 
   * @return the time required by the proof-search.
   */
  public long proofSearchTime() {
    return proof_search_end_time - proof_search_start_time;
  }

}
