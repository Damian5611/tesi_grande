package jtwbprolog.launcher;

import org.apache.commons.logging.impl.SimpleLog;

public class LauncherExecConfiguration {

  private int log_mode = SimpleLog.LOG_LEVEL_OFF;
  private LauncherExecMode executionMode = LauncherExecMode.PLAIN;
  private String[] inputFileNames = null;

  /**
   * @return the lOG_MODE
   */
  public int logMode() {
    return this.log_mode;
  }

  /**
   * @param log_mode the lOG_MODE to set
   */
  void setLogMode(int logMode) {
    this.log_mode = logMode;
  }

  public LauncherExecMode executionMode() {
    return executionMode;
  }

  void setExecutuionMode(LauncherExecMode mode) {
    this.executionMode = mode;
  }

  public boolean verboseMode() {
    return this.executionMode == LauncherExecMode.VERBOSE;
  }

  public boolean debugMode() {
    return this.log_mode == SimpleLog.LOG_LEVEL_DEBUG;
  }

  /**
   * @return the inputFileNames
   */
  public String[] getInputFileNames() {
    return this.inputFileNames;
  }

  /**
   * @param inputFileNames the inputFileNames to set
   */
  public void setInputFileNames(String[] inputFileNames) {
    this.inputFileNames = inputFileNames;
  }

  //  private File createLogDir() {
  //    File logDir = new File(LOG_DIR_NAME);
  //    if (!logDir.exists()) {
  //      try {
  //        logDir.mkdir();
  //        return logDir;
  //      } catch (SecurityException e) {
  //        MSGManager.error(MSG.LAUNCHER.ERROR.LOG_DIR_CANNOT_BE_CREATED, logDir.getAbsolutePath(),
  //            e.getMessage());
  //        return null;
  //      }
  //    } else {
  //      if (!logDir.isDirectory()) {
  //        MSGManager.error(MSG.LAUNCHER.ERROR.LOG_DIR_IS_NOT_A_DIR, logDir.getAbsolutePath());
  //        return null;
  //      }
  //    }
  //    return logDir;
  //  }

}
