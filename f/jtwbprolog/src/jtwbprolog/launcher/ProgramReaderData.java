package jtwbprolog.launcher;

import jtwbprolog.program.Program;

public class ProgramReaderData {

  public ProgramReaderData(Program program, long parsing_problem_start_time,
      long parsing_problem_end_time) {
    super();
    this.program = program;
    this.parsing_problem_start_time = parsing_problem_start_time;
    this.parsing_problem_end_time = parsing_problem_end_time;
  }

  private Program program;
  private long parsing_problem_start_time = 0;
  private long parsing_problem_end_time = -1;

  /**
   * @return the program
   */
  public Program getProgram() {
    return this.program;
  }

  /**
   * @return the parsing_problem_start_time
   */
  public long getParsing_problem_start_time() {
    return this.parsing_problem_start_time;
  }

  /**
   * @return the parsing_problem_end_time
   */
  public long getParsing_problem_end_time() {
    return this.parsing_problem_end_time;
  }

  /**
   * Returns the time required to parse the problem description in milliseconds.
   * 
   * @return the time needed to parse the input problem.
   */
  public long problemParsingTime() {
    return parsing_problem_end_time - parsing_problem_start_time;
  }
}
