package jtwbprolog.launcher;

import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.impl.SimpleLog;

import jtabwb.engine.Engine;
import jtabwb.util.ImplementationError;
import jtwbprolog.parser.JTWBPrologReader;
import jtwbprolog.program.Program;
import jtwbprolog.tp.TPProlog;
import jtwbprolog.util.MSGManager;

/**
 * Launcher for JTabWbProlog.
 * 
 * @author Mauro Ferrari
 */
public class Main {

  private final Log LOG;
  private ThreadMXBean bean;

  private Main() {
    this.LOG = new SimpleLog(this.getClass().getCanonicalName());
    this.bean = ManagementFactory.getThreadMXBean();
  }

  /*
   * Read the specified source program and returns its internal representation.
   */
  private ProgramReaderData readFromFile(LauncherExecConfiguration launcherExecConf,
      String inputFilename) {
    try {
      // get the file
      File inputFile = new File(inputFilename);
      if (!inputFile.exists()) {
        LOG.error(String.format(MSG.LAUNCHER.ERROR.NO_SUCH_FILE, inputFilename));
        System.exit(1);
      }
      info_preReaderExecutionDetails(launcherExecConf);
      MSGManager.infoNoLn(MSG.LAUNCHER.INFO.PROBLEM_DESCRIPTION_PARSING_BEGIN, inputFile.getPath());

      // build the program reader
      JTWBPrologReader programReader = new JTWBPrologReader(inputFile);
      // read the probgram
      long parsing_problem_start_time = getCurrentTimeMilleseconds();
      Program program = programReader.read();
      long parsing_problem_end_time = getCurrentTimeMilleseconds();

      // build data bundle
      ProgramReaderData programReaderData =
          new ProgramReaderData(program, parsing_problem_start_time, parsing_problem_end_time);

      MSGManager.info(MSG.LAUNCHER.INFO.PROBLEM_DESCRIPTION_PARSING_END,
          programReaderData.problemParsingTime());
      return programReaderData;
    } catch (IOException e) {
      MSGManager.error(MSG.LAUNCHER.ERROR.IO_EXCEPTION, e.getMessage());
      System.exit(1);
    }
    return null;
  }

  private long getCurrentTimeMilleseconds() {
    return TimeUnit.MILLISECONDS.convert(bean.getCurrentThreadCpuTime(), TimeUnit.NANOSECONDS);
  }

  /**
   * The method launching the prover from main.
   * 
   * @param args the command line arguments.
   */
  private void start(String[] args) {
    LauncherExecConfiguration launcherExecConf;
    launcherExecConf = (new CmdLineOptions()).processCommandLineOptions(args);
    // configure
    ((SimpleLog) LOG).setLevel(launcherExecConf.logMode());
    for (String inputFilename : launcherExecConf.getInputFileNames()) {
      // read problem
      ProgramReaderData readerData = readFromFile(launcherExecConf, inputFilename);
      print_programDetails(readerData.getProgram());
      ProofSearchData proofSearchData = searchProof(launcherExecConf, readerData.getProgram());
      print_postProofSearchDetails(proofSearchData, launcherExecConf);
    }
  }

  private ProofSearchData searchProof(LauncherExecConfiguration launcherExecConf,
      Program program) {

    TPProlog prover = new TPProlog(program);

    MSGManager.info(MSG.PROOF_SEARCH_INFO.PROVER_DETAILS, prover.getProverName());
    MSGManager.info(MSG.LAUNCHER.INFO.PROVING_BEGIN);

    Engine.ExecutionMode engineExecMode = null;
    switch (launcherExecConf.executionMode()) {
    case PLAIN:
      engineExecMode = Engine.ExecutionMode.ENGINE_PLAIN;
      break;
    case VERBOSE:
      engineExecMode = Engine.ExecutionMode.ENGINE_VERBOSE;
    default:
      throw new ImplementationError(ImplementationError.CASE_NOT_IMPLEMENTED_arg,
          launcherExecConf.executionMode());
    }

    Engine engine = new Engine(prover, program.getGoal(), engineExecMode);
    // start the proof search
    long proof_search_start_time = getCurrentTimeMilleseconds();
    engine.searchProof();
    // end of proof search, set info values
    long proof_search_end_time = getCurrentTimeMilleseconds();

    return new ProofSearchData(engine,prover, proof_search_start_time, proof_search_end_time);
  }

  private void info_preReaderExecutionDetails(LauncherExecConfiguration conf) {
    MSGManager.info(MSG.PROOF_SEARCH_INFO.READER_DETAILS, "jtabwb");
  }

  private void print_programDetails(Program program) {

  }

  private void print_postProofSearchDetails(ProofSearchData proofSearchData,
      LauncherExecConfiguration launcherConfiguration) {
    StringBuilder sb = new StringBuilder();

    sb.append(MSG.PROOF_SEARCH_INFO.INFO_SEPARATOR);
    sb.append("\n");
    sb.append(String.format(MSG.PROOF_SEARCH_INFO.PROVER_DETAILS,
        proofSearchData.getProver().getProverName()));
    sb.append("\n");

    //    // ADD proof search details
    //    {
    //      StringBuilder psdsb = proofSearchData.proofSearchDetails().getDetailsDescription();
    //      String[] lines = psdsb.toString().split("\\n");
    //      for (String line : lines)
    //        sb.append(
    //            String.format(MSG.PROOF_SEARCH_INFO.PROOF_SEARCH_DETAILS_LINE_PREFIX, line) + "\n");
    //    }
    //
    //    long proofSearch_time = proofSearchData.proofSearchTime();
    //    long buildInitialNodeSet_time = proofSearchData.initialNodeSetConstructionTime();
    //    long problemReading_time = proofSearchData.problemParsingTime();
    //    long totalProof_time = buildInitialNodeSet_time + proofSearch_time + problemReading_time;
    //    sb.append(String.format(MSG.PROOF_SEARCH_INFO.TIMINGS_DETAILS, proofSearch_time,
    //        buildInitialNodeSet_time, problemReading_time));
    //    sb.append("\n");
    //    String convertedTime = buildTimeString(totalProof_time);
    //    if (convertedTime == null)
    //      sb.append(String.format(MSG.PROOF_SEARCH_INFO.TOTAL_TIME_1, totalProof_time));
    //    else
    //      sb.append(String.format(MSG.PROOF_SEARCH_INFO.TOTAL_TIME_2, totalProof_time,
    //          convertedTime.toString()));
    //    sb.append("\n");
    //
    //    sb.append(MSG.PROOF_SEARCH_INFO.INFO_SEPARATOR);

    MSGManager.info(sb.toString());
  }

  private String buildSecondBasedString(long miliSeconds) {
    int sec = (int) TimeUnit.MILLISECONDS.toSeconds(miliSeconds);
    int mil = (int) miliSeconds % 1000;

    return String.format(MSG.LAUNCHER.TIME_STR.JTABWB_TIME_STRING, sec, mil);
  }

  /**
   * Launch the application with the arguments specified on the command line.
   * 
   * @param args the command line arguments.
   */
  public static void main(String[] args) {
    Main main = new Main();
    main.start(args);
  }

  private String buildTimeString(long miliSeconds) {
    int hrs = (int) TimeUnit.MILLISECONDS.toHours(miliSeconds);
    int min = (int) TimeUnit.MILLISECONDS.toMinutes(miliSeconds) % 60;
    int sec = (int) TimeUnit.MILLISECONDS.toSeconds(miliSeconds) % 60;
    int mil = (int) miliSeconds % 1000;

    if (hrs == 0 && min == 0 && sec == 0)
      return null;
    else
      return String.format("%02d:%02d:%02d + %d", hrs, min, sec, mil);
  }

}
