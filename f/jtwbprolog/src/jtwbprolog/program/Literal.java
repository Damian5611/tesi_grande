package jtwbprolog.program;

import jtabwb.engine._AbstractFormula;
import jtabwb.engine._AbstractGoal;

public class Literal implements _AbstractFormula, _AbstractGoal {

  Predicate predicate;
  Term[] args;

  public Literal(Predicate predicate, int numArgs) {

    this.predicate = predicate;
    args = new Term[numArgs];

  }
  
  public Literal(Predicate predicate, Term[] args) {
	  
	  this.predicate = predicate;
	  this.args = args;
	  
  }


  public Term[] getArgs() {

    return args;

  }
  
  public Predicate getPredicate() {
	  
	  return predicate;
	  
  }

  public void addArg(Term arg, int index) {

    args[index] = arg;

  }

  public String toString() {

    String result = predicate.getName();
    result += "(";

    for (int i = 0; i < args.length; i++) {

      result += args[i].toString();

      if (i + 1 != args.length)
        result += ",";

    }

    result += ")";

    return result;

  }

  @Override
  public String format() {
    return this.toString();
  }

  @Override
  public String shortName() {
    return "Literal";
  }

  @Override
  public Literal clone() {
    Literal result = new Literal(predicate, args.length);
    result.args = this.args;
    return result;
  }

}
