package jtwbprolog.program;

import jtabwb.engine._AbstractGoal;

public class Program {

  private _Clause[] clauses;
  private Goal goal;

  public Program(Goal goal, int numClauses) {

    this.goal = goal;
    clauses = new _Clause[numClauses];

  }

  public void addClause(_Clause clause, int index) {

    clauses[index] = clause;

  }

  public _Clause[] getClauses() {

    return clauses;
  }
  
  public Goal getGoal(){
    return goal;
  }

  public String toString() {

    String result = "";

    for (int i = 0; i < clauses.length; i++) {

      result += i + 1 + " " + clauses[i].toString();

      if (i + 1 != clauses.length)
        result += "\n";

    }

    result += "\n" + (clauses.length + 1) + goal.toString();

    return result;

  }

  public String format() {
    return this.toString();
  }

}
