package jtwbprolog.program;

public interface _Clause {

	public boolean isUnit();
	
	public Literal[] premises();
	
}
