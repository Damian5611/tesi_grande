package jtwbprolog.program;

public class UnitClause implements _Clause {

  private Literal conclusion;

  public UnitClause(Literal literal) {

    conclusion = literal;

  }

  public Literal getLiteral() {

    return conclusion;

  }

  @Override
  public Literal[] premises() {
    return null;
  }

  public boolean isUnit() {

    return true;

  }

  public String toString() {

    return conclusion.toString() + ".";

  }

}
