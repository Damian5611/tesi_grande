package jtwbprolog.program;

public class DefiniteClause implements _Clause {

  private Literal conclusion;
  private Literal[] premises;

  public DefiniteClause(Literal conclusion, int numPremises) {

    this.conclusion = conclusion;
    premises = new Literal[numPremises];

  }

  public void addPremise(Literal premise, int index) {

    premises[index] = premise;

  }

  public boolean isUnit() {

    return false;

  }

  @Override
  public Literal[] premises() {
    return premises;
  }

  public String toString() {

    String s = "";

    for (int i = 0; i < premises.length; i++) {

      s += premises[i].toString();

      if (i + 1 != premises.length)
        s += ",";

    }

    return conclusion.toString() + " :- " + s + ".";

  }

}
