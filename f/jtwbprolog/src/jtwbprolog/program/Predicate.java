package jtwbprolog.program;

public class Predicate {

	private String name;
	private int numArgs;
	
	public Predicate(String name, int numArgs) {
		
		this.name = name;
		this.numArgs = numArgs;
		
	}
	
	public String getName() {
		
		return name;
		
	}
	
	public int getNumArgs() {
		
		return numArgs;
		
	}
	
}
