package jtwbprolog.program;

public class Variable implements Term {

  private String name;

  public Variable(String name) {

    this.name = name;

  }

  public String toString() {

    return name;

  }

  public boolean isFunction() {

    return false;

  }

  public boolean isVariable() {

    return true;

  }

  public Function getFunction() {

    return null;

  }

  public Term[] getArgs() {

    return null;

  }

  public void addArgument(Term arg, int index) {
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Variable other = (Variable) obj;
    if (this.name == null) {
      if (other.name != null)
        return false;
    } else if (!this.name.equals(other.name))
      return false;
    return true;
  }

}
