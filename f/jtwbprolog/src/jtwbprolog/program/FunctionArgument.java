package jtwbprolog.program;

public class FunctionArgument implements Term {
	
	private Function function;
	private Term[] args;

	public FunctionArgument(Function function, int numArgs) {
		
		this.function = function;
		args = new Term[numArgs];
		
	}
	
    public FunctionArgument(Function function, Term[] args) {
		
		this.function = function;
		this.args = args;
		
	}
	
	public void addArgument(Term arg, int index) {
		
		args[index] = arg;
		
	}
	
    public String toString() {
		
		String result = function.getName();
		result += "(";
		
		for(int i = 0; i < args.length; i++) {
			
			result += args[i].toString();
			
			if(i + 1 != args.length)
				result += ",";
			
		}
		
		result += ")";
		
		return result;
		
	}
    
    public Function getFunction() {
    	
    	return function;
    	
    }
    
    public Term[] getArgs() {
    	
    	return args;
    	
    }
	
	public boolean isFunction() {
		
		return true;
		
	}
	
	public boolean isVariable() {
		
		return false;
		
	}
	
}
