package jtwbprolog.program;

public class Constant implements Term {
	
   private String name;
	
	public Constant(String name) {
		
		this.name = name; 
		
	}
	
   public String toString() {
		
		return name;
		
	}
	
	public boolean isFunction() {
		
		return false;
		
	}
	
	public boolean isVariable() {
		
		return false;
		
	}
	
	public void addArgument(Term arg, int index) {};
	
	public Function getFunction() {
		
		return null;
		
	}
	
	public Term[] getArgs() {
		
		return null;
		
	}

}
