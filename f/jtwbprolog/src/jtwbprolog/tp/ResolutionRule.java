package jtwbprolog.tp;

import jtabwb.engine._AbstractFormula;
import jtabwb.engine._AbstractGoal;
import jtabwb.engine._RegularRule;
import jtwbprolog.program._Clause;
import jtwbprolog.program.Literal;
import jtwbprolog.program.Substitution;

/**
 * Implements an instance of the resolution rule.
 *
 */
public class ResolutionRule implements _RegularRule {

  public ResolutionRule(Literal goalLiteral, _Clause clause, Substitution mgu) {
    super();
    this.goalLiteral = goalLiteral;
    this.premises = clause.premises();
    this.mgu = mgu;
  }

  private Literal goalLiteral;
  private Literal[] premises;
  private Substitution mgu;
  private int nextPremiseToTreat = 0;

  @Override
  public String name() {
    return "ResSLD";
  }

  @Override
  public boolean hasNextSubgoal() {
    return nextPremiseToTreat < this.premises.length;
  }

  @Override
  public _AbstractFormula mainFormula() {
    return mgu.apply(premises[nextPremiseToTreat++]);
  }

  @Override
  public _AbstractGoal nextSubgoal() {
    return premises[nextPremiseToTreat++];
  }

  @Override
  public int numberOfSubgoals() {
    return premises.length;
  }

}
