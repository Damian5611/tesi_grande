package jtwbprolog.tp;

import jtabwb.engine.ProofSearchResult;
import jtabwbx.prop.formula.Formula;

public class ProofSearchDetails {

  private int iterations;
  private int numberOfAppliedRules;
  private ProofSearchResult proofSearchResult;
  private Formula goal;

  /**
   * Returns the goal of the proof-search.
   * 
   * @return
   */
  public Formula getGoal() {
    return goal;
  }

  /**
   * 
   * @return the numperOfIterations
   */
  public int getNumberOfIterations() {
    return this.iterations;
  }

  /**
   * @return the number of applied rules.
   */
  public int getNumberOAppliedRules() {
    return numberOfAppliedRules;
  }

  /**
   * @return the proofSearchResult
   */
  public ProofSearchResult getProofSearchResult() {
    return this.proofSearchResult;
  }

  /**
   * Returns the number of rules applied performing the proof-search.
   * 
   * @return the number of applied rules.
   */
  public int getNumberOfRuleApplications() {
    return numberOfAppliedRules;
  }

  /**
   * @param iterations the iterations to set
   */
  public void setIterations(int iterations) {
    this.iterations = iterations;
  }

  /**
   * @param numberOfAppliedRules the numberOfAppliedRules to set
   */
  public void setNumberOfAppliedRules(int numberOfAppliedRules) {
    this.numberOfAppliedRules = numberOfAppliedRules;
  }

  /**
   * @param proofSearchResult the proofSearchResult to set
   */
  public void setProofSearchResult(ProofSearchResult proofSearchResult) {
    this.proofSearchResult = proofSearchResult;
  }

  /**
   * @param goal the goal to set
   */
  public void setGoal(Formula goal) {
    this.goal = goal;
  }

}
