package jtwbprolog.tp;

import jtabwb.engine.ProofSearchResult;
import jtabwb.engine.ProvabilityStatus;
import jtabwb.engine.ProverName;
import jtabwb.engine._Prover;
import jtabwb.engine._Strategy;
import jtabwb.util.ImplementationError;
import jtwbprolog.program.Program;

public class TPProlog implements _Prover {

  private static String NAME = "jtwbprolog";
  private static String VERSION = "0.1";
  private static String DESCRIPTION = "JTabWb implementatio of SLD-resolution";

  public TPProlog(Program program) {
    this.PROVER_NAME = new ProverName(NAME, VERSION, null, DESCRIPTION);
    this.PROGRAM = program;
    this.STRATEGY = new SLDStrategy(program);
  }

  private final ProverName PROVER_NAME;
  private final _Strategy STRATEGY;
  private final Program PROGRAM;

  @Override
  public ProverName getProverName() {
    return PROVER_NAME;
  }

  @Override
  public _Strategy getStrategy() {
    return STRATEGY;
  }

  @Override
  public ProvabilityStatus statusFor(ProofSearchResult psresult) {
    switch (psresult) {
    case FAILURE:
      return ProvabilityStatus.UNPROVABLE;
    case SUCCESS:
      return ProvabilityStatus.UNPROVABLE;
    default:
      throw new ImplementationError(ImplementationError.CASE_NOT_IMPLEMENTED_arg,
          psresult.toString());
    }
  }

  public Program getProgram() {
    return PROGRAM;
  }

}
